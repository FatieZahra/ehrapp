Rails.application.routes.draw do
  resources :items

  resources :consultants

  root "consultants#index"
end
